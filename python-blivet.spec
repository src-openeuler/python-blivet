%bcond_with enable_python2
%bcond_without python3

Name:           python-blivet
Version:        3.8.2
Release:        9
Epoch:          1
Summary:        A python module for system storage configuration
License:        LGPL-2.1-or-later
URL:            https://storageapis.wordpress.com/projects/blivet
Source0:        http://github.com/storaged-project/blivet/releases/download/blivet-%{version}/blivet-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  systemd gettext python3-devel python3-setuptools

%if %{with enable_python2}
BuildRequires:  python2-devel python2-setuptools
%endif

Patch0:         fix-the-long-hostname.patch

%ifarch sw_64
patch9001:      blivet-3.4.2-sw.patch
%endif
patch9002:      Incomplete-Chineseization-of-disk-mount.patch

patch6001:      backport-Ignore-invalid-empty-UUIDs-for-NVMe-namespaces.patch
patch9003:      bugfix-fix-empty-UUIDs-for-NVMe-namespaces.patch
patch9004:      bugfix-revert-Prefer-UUID-for-fstab-spec-for-DM-devices-too.patch
patch6002:      backport-Fix-intel-biosraid-cant-get-device-name-causing-crashed.patch

%description
The python-blivet package is a python module for examining and modifying
storage configuration.

%package     -n blivet-data
Summary:        Data packages for python-blivet
Conflicts:      python-blivet < 1:2.0.0 python3-blivet < 1:2.0.0
Obsoletes:      python2-blivet

%description -n blivet-data
Data packages for python-blivet.

%package     -n python3-blivet
Summary:        python3 package for blivet
Requires:       python3 python3-six python3-pyudev >= 0.18 python3-pyparted >= 3.10.4
Requires:       libselinux-python3 python3-blockdev >= 3.0 parted >= 1.8.1
Requires:       python3-bytesize >= 0.3 util-linux >= 2.15.1 lsof python3-gobject-base
Requires:       systemd-udev blivet-data = %{epoch}:%{version}-%{release}
Recommends:     libblockdev-btrfs >= 3.0 libblockdev-crypto >= 3.0 libblockdev-dm >= 3.0
Recommends:     libblockdev-lvm >= 3.0 libblockdev-mdraid >= 3.0 libblockdev-mpath >= 3.0
Recommends:     libblockdev-loop >= 3.0 libblockdev-swap >= 3.0
Recommends:     fcoe-utils
%ifarch s390 s390x
Recommends:     libblockdev-s390 >= 3.0 
%endif
%{?python_provide:%python_provide python3-blivet}
Obsoletes:      blivet-data < 1:2.0.0
%if %{without enable_python2}
Obsoletes:      python2-blivet < 1:2.0.2-2 python-blivet < 1:2.0.2-2
%else
Obsoletes:      python-blivet < 1:2.0.0
%endif

%description -n python3-blivet
python3 package for blivet

%if %{with enable_python2}
%package     -n python2-blivet
Summary:        python2 package for blivet
Requires:       python2 python2-six python2-pyudev >= 0.18 python2-pyparted >= 3.10.4
Requires:       python2-libselinux python2-blockdev >= 2.19 parted >= 1.8.1
Requires:       python2-bytesize >= 0.3 util-linux >= 2.15.1 lsof python2-gobject-base
Requires:       systemd-udev blivet-data = %{epoch}:%{version}-%{release} python2-hawkey
Recommends:     libblockdev-btrfs >= 2.19 libblockdev-crypto >= 2.19 libblockdev-dm >= 2.19
Recommends:     libblockdev-fs >= 2.19 libblockdev-kbd >= 2.19 libblockdev-loop >= 2.19
Recommends:     libblockdev-lvm >= 2.19 libblockdev-mdraid >= 2.19 libblockdev-mpath >= 2.19
Recommends:     libblockdev-nvdimm >= 2.19 libblockdev-part >= 2.19 libblockdev-swap >= 2.19
Recommends:     libblockdev-s390 >= 2.19
%{?python_provide:%python_provide python2-blivet}
Obsoletes:      blivet-data < 1:2.0.0 python-blivet < 1:2.0.0

%description -n python2-blivet
python2 package for blivet
%endif

%package_help

%prep
%autosetup -n blivet-%{version} -p1

%build

%if %{with enable_python2}
make PYTHON=%{__python2}
%endif
%{?with_python3:make PYTHON=%{__python3}}

%install
%if %{with enable_python2}
make PYTHON=%{__python2} DESTDIR=%{buildroot} install
%endif
%{?with_python3:make PYTHON=%{__python3} DESTDIR=%{buildroot} install}

%find_lang blivet

%files       -n blivet-data -f blivet.lang
%defattr(-,root,root)
%{_sysconfdir}/dbus-1/system.d/*
%{_libexecdir}/*
%{_unitdir}/*
%{_datadir}/dbus-1/system-services/*

%files       -n python3-blivet
%defattr(-,root,root)
%license COPYING
%doc examples
%{python3_sitelib}/*

%if %{with enable_python2}
%files       -n python2-blivet
%defattr(-,root,root)
%license COPYING
%doc examples
%{python2_sitelib}/*
%endif

%files          help
%defattr(-,root,root)
%doc README.md

%changelog
* Wed Dec 04 2024 sunhai<sunhai10@huawei.com> - 1:3.8.2-9
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix anaconda partition error tip

* Tue Oct 22 2024 sunhai<sunhai10@huawei.com> - 1:3.8.2-8
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Fix intel biosraid can't get device name causing crashed

* Sat Jun 29 2024 sunhai<sunhai10@huawei.com> - 1:3.8.2-7
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:revert Prefer UUID for fstab spec for DM devices too

* Mon Jun 17 2024 yueyuankun <yueyuankun@kylinos.cn> - 1:3.8.2-6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Add Recommends fcoe-utils

* Fri May 10 2024 sunhai<sunhai10@huawei.com> - 1:3.8.2-5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix empty UUIDs for NVMe namespaces

* Mon Apr 15 2024 sunhai<sunhai10@huawei.com> - 1:3.8.2-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Ignore invalid/empty UUIDs for NVMe namespaces

* Tue Mar 26 2024 sunhai<sunhai10@huawei.com> - 1:3.8.2-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix lvs lost

* Wed Feb 28 2024 sunhai<sunhai10@huawei.com> - 1:3.8.2-2
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:fix get libbd_lvm.so error

* Sat Feb 17 2024 sunhai<sunhai10@huawei.com> - 1:3.8.2-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update to 3.8.2

* Fri Sep 1 2023 Wanli Niu <niuwanli@cysoftware.com.cn> - 1:3.6.1-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix setting kickstart data

* Mon Feb 13 2023 hanhuihui<hanhuihui5@huawei.com> - 1:3.6.1-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add udf-filesystem support

* Sat Feb 11 2023 Wenlong Zhang<zhangwenlong@loongson.cn> - 1:3.6.1-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add loongarch64 support for blivet

* Mon Jan 30 2023 hanhuihui<hanhuihui5@huawei.com> - 1:3.6.1-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update to 3.6.1

* Tue Dec 27 2022 hanhuihui<hanhuihui5@huawei.com> - 1:3.4.2-5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Exclude unusable disks from PartitionFactory

* Fri Dec 09 2022 wanglimin<wanglimin@xfusion.com> - 1:3.4.2-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: Incomplete Chineseization of disk mount

* Tue Oct 18 2022 wuzx<wuzx1226@qq.com> - 3.4.2-3
- add sw64 patch

* Thu Jul 28 2022 Chenxi Mao <chenxi.mao@suse.com> - 3.4.2-2
- Fix remove subvolumes error on brtfs during installation.

* Wed Dec 15 2021 yangcheng <yangcheng87@huawei.com> - 3.4.2-1
- DESC:Upgrade to 3.4.2

* Fri Dec 10 2021 yanan <yanan@huawei.com> - 3.3.2-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix the failure of not a valid name for this device while the hostname is setted as 63 maxlen

* Sat Oct 30 2021 yanan <yanan@huawei.com> - 3.3.2-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Convert LVM filter lists to sets
       Remove action device from LVM reject list

* Thu Jun 10 2021 liuxin <liuxin264@huawei.com> - 3.3.2-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix allocate_partitions threw an exception while adding /boot to RAID disk

* Wed Feb 03 2021 gaihuiying <gaihuiying1@huawei.com> - 3.3.2-1
- Type:requirement
- ID:NA
- SUG:NA
- DESC:update python-blivet to 3.3.2

* Wed Nov 18 2020 gaihuiying <gaihuiying1@huawei.com> - 3.2.2-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add %bcond_without python3 for building in docker

* Tue Sep 15 2020 liuxin <liuxin264@huawei.com> - 3.2.2-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:bugfix raid1 dirvice cann't show when install

* Thu Jul 9 2020 zhangqiumiao <zhangqiumiao1@huawei.com> - 3.2.2-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:bugfix about changelog in python-blivet.spec

* Thu Jun 18 2020 zhangqiumiao <zhangqiumiao1@huawei.com> - 3.2.2-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:upgrate to version 3.2.2

* Sat Mar 21 2020 songnannan <songnannan@huawei.com> - 3.1.1-8
- bugfix about update

* Sat Mar 21 2020 hexiujun <hexiujun1@huawei.com> - 3.1.1-7
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:check if disklabel supports partition names

* Sat Mar 14 2020 hexiujun <hexiujun1@huawei.com> - 3.1.1-6
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:disable python2

* Mon Jan 13 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.1.1-5
- update software package

* Sun Dec 29 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.1.1-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:optimization the spec

* Wed Oct 23 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.1.1-3
- Package init
